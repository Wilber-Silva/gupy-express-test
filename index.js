const express = require('express')
// const pipedrive = require('pipedrive')
const bodyParser = require('body-parser')
const routes = require('./src/routes')
const app = express()

app.use(bodyParser.json())

routes(app)

app.listen(3000, function () {
  console.log('Listening on port 3000!')
});