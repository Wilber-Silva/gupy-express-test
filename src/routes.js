const ExampleController = require('./Controllers/ExampleController.js.js')
const DemandController = require('./Controllers/DemandController')

module.exports = (app) => {
    app.post('/', ExampleController.create)
    app.get('/', ExampleController.list)
    app.put('/:_id', ExampleController.update)


    app.get('/bling', DemandController.list)
    app.post('/bling', DemandController.create)
}