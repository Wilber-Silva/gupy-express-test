const { ResponseOK, ResponseError } = require('will-core-lib/http')
const jsonToXml = require('jsontoxml')
const BlingIntegration = require('../Integrations/BlingIntegration')

const { json } = require('../express-respose-adapter')

class ExampleController{
    list = async (req, res) => {
        try {
            const items = await BlingIntegration.getDemand()
            return json(res, new ResponseOK(items))
        } catch(error) {
            return json(res, new ResponseError(error))
        }
    }
    create = async ({ body }, res) => {
        try {
            const xml = await jsonToXml(body)
            const created = await BlingIntegration.createDemand(xml)
            return json(res, new ResponseOK(created))
        } catch (error) {
            return json(res, new ResponseOK(error.response.data))
        }
    }
}

module.exports = new ExampleController()