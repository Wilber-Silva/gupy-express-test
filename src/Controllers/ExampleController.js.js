const { ResponseOK, ResponseError } = require('will-core-lib/http')
const ExampleRepository =  require('../Repositories/example-repository')

const { json } = require('../express-respose-adapter')

class ExampleController{
    create = async({ body }, res) => {
        try{
            const created = await ExampleRepository.create(body)
            return json(res, new ResponseOK(created))
        } catch(error){
            return json(res, new ResponseError(error))
        }
    }
    list = async (req, res) => {
        try {
            const examples = await ExampleRepository.find({visible: true})
            return json(res, new ResponseOK(examples))
        } catch(error) {
            return json(res, new ResponseError(error))
        }
    }
    update = async ({ params, body }, res) => {
        try {
            const updated = await ExampleRepository.update({ _id: params._id }, body)
            return json(res, new ResponseOK(updated))
        } catch (error) {
            return json(res, new ResponseError(error))
        }
    }
}

module.exports = new ExampleController()