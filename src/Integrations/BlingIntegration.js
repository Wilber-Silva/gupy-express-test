const { BaseIntegration } = require('./BaseIntegration')

class BlingIntegration extends BaseIntegration{
    constructor() {
        super({
            baseURL: 'https://bling.com.br/Api/v2', 
            apiKey :'81cd93a20eaa7ca3021f514b280b79c6e4f3cc6b910027de72d9e19adc967984ff1f4e33'
        })
    }
    getDemandById =  async (id) => {
        const { data } = await this.get(`/pedidos/${id}/json`)
        return data
    }
    getDemand =  async () => {
        const { data } = await this.get('/pedidos/json')
        return data.retorno
    }
    getProducts = async () => {
        const { data } = await this.get('/produtos/json')
    }
    createDemand = async (body) => {
        const { data } = await this.post('/pedidos/json', { xml: body })
        return data.retorno
    }
}

module.exports = new BlingIntegration()