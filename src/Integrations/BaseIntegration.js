const axios = require('axios')

class BaseIntegration {
    constructor({ baseURL, apiKey }) {
        this.http = axios.create({
            baseURL
            // , apikey: apiKey
        })
        this.apiKey = apiKey
    }

    get = async (uri, params = {}) => {
        params.apikey = this.apiKey	
        return this.http.get(uri, { params })
    }
    post = async (uri, data, params = {}) => {
        params.apikey = this.apiKey	
        return this.http.post(uri, data, { params })
    }
}

module.exports.BaseIntegration = BaseIntegration