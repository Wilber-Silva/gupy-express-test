const mongoose = require('mongoose')

module.exports.ExampleSchema = new mongoose.Schema({
    name: { type: String },
	visible: { type: Boolean, default: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    deletedAt: { type: Date, default: null }
})
