const { BaseRepository, MongoConnection } = require('will-core-lib/connectors/mongodb')
const { ExampleSchema } = require('./../Models/example-model')

const db = 'mongodb://wcosta:413105839@cluster0-shard-00-00-sz79j.mongodb.net:27017,cluster0-shard-00-01-sz79j.mongodb.net:27017,cluster0-shard-00-02-sz79j.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'

class ExampleRepository extends BaseRepository{
    constructor() {
        super('example', ExampleSchema,new MongoConnection(db))
    }
}

module.exports = new ExampleRepository()